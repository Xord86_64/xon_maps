textures/qcdm3/hip_sky
{
	qer_editorimage env/vale/vale_dn.jpg
	q3map_lightmapFilterRadius 0 32
//		      R   G   B  Int Deg Elev
	q3map_sunExt .92 .92 .82 70 220 87 2 32
	q3map_sunExt .89 .98 1 50 0 60 3 12
	q3map_sunExt .89 .98 1 50 90 55 3 12
	q3map_sunExt .89 .98 1 50 160 55 3 12
	q3map_sunExt .89 .98 1 50 290 57 3 12
	q3map_lightimage env/vale/vale_ft.jpg
	q3map_skyLight 20 3
	q3map_noFog
	q3map_globalTexture
	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	skyparms env/vale/vale - -
	nopicmip	
}

textures/qcdm3/hip_sky_void
{
	qer_editorimage textures/qcdm3/clouds.tga
	q3map_lightmapFilterRadius 0 32
//		      R  G  B IntDegElev
//	q3map_sunExt .9 .9 .8 1 135 60 2 32
//	q3map_sunExt .9 .9 .8 1 315 70 2 32
	q3map_lightimage textures/qcdm3/clouds2.jpg
	q3map_skyLight 1 3
	q3map_noFog
	q3map_globalTexture
	surfaceparm sky
	surfaceparm noimpact
	surfaceparm nolightmap
	skyparms - 512 -
	nopicmip
	{
		map textures/qcdm3/clouds.tga
		tcMod scroll 0.0150 -0.0050
		tcmod scale 5 5
		rgbGen identityLighting
	}
	{
		map textures/qcdm3/clouds2.tga
		tcMod scroll 0.03 -0.02
		blendFunc blend
		tcmod scale 3 3
		rgbGen identityLighting
	}		
}

textures/qcdm3/cliff_bd
{
	qer_alphafunc greater 0.5
	qer_trans 1
	surfaceparm nolightmap
	surfaceparm trans	
	nopicmip
	noMipMaps
	cull none
	{
		map textures/qcdm3/cliff_bd.tga
		blendFunc blend
		rgbGen identitylighting
	}
}

textures/qcdm3/orange
{
	qer_editorimage textures/qcdm3/orange.jpg
	q3map_globaltexture
	q3map_lightRGB .82 .63 .3
	surfaceparm noimpact
	surfaceparm water
        q3map_lightmapSampleOffset -8
	q3map_surfacelight 400
        q3map_backSplash 0 4
        nopicmip
	tesssize 32
	cull disable
	{
		map textures/qcdm3/orange.jpg
		tcMod turb 0 .08 0 .05
	}
}

textures/qcdm3/ph_duel
{
	cull none
	surfaceparm alphashadow
	surfaceparm trans
	surfaceparm nomarks
	nopicmip
        {
                map textures/qcdm3/ph_duel.tga
                alphaFunc GE128
		depthWrite
        }
        {
		map $lightmap
		rgbGen identity
		blendFunc filter
		depthFunc equal
	}
}

textures/qcdm3/ph_turb
{
	qer_editorimage textures/qcdm3/ph_turb.tga
        surfaceparm trans	
        surfaceparm nomarks	
	cull none
        nopicmip
	{
		clampmap textures/qcdm3/ph_turb.tga
		tcMod rotate 100
		blendFunc GL_ONE GL_ZERO
		alphaFunc GE128
		depthWrite
		rgbGen identity
	}
	{
		map $lightmap
		rgbGen identity
		blendFunc GL_DST_COLOR GL_ZERO
		depthFunc equal
	}
}

textures/qcdm3/logo_genArc
{
    qer_editorImage textures/qcdm3/logo_genArc.tga
    surfaceparm nonsolid
    surfaceparm nodlight
    surfaceparm nomarks
    surfaceparm trans
    surfaceparm detail
    polygonOffset
    nopicmip
    {
        map textures/qcdm3/logo_genArc.tga
        rgbGen const ( .37 .37 .37 )
        blendFunc add
    }
}

textures/qcdm3/dec_spot4
{
   polygonOffset
   surfaceparm nonsolid
   surfaceparm nomarks
   nopicmip
   {
      map textures/qcdm3/dec_spot4.tga
      blendFunc GL_ZERO GL_ONE_MINUS_SRC_COLOR
      detail
   }
}

textures/qcdm3/dec_tab_d
{
   polygonOffset
   surfaceparm nonsolid
   surfaceparm nomarks
   nopicmip
   {
      map textures/qcdm3/dec_tab_d.tga
      blendFunc GL_ZERO GL_ONE_MINUS_SRC_COLOR
      detail
   }
}

textures/qcdm3/dec_tab_e
{
   polygonOffset
   surfaceparm nonsolid
   surfaceparm nomarks
   nopicmip
   {
      map textures/qcdm3/dec_tab_e.tga
      blendFunc GL_ZERO GL_ONE_MINUS_SRC_COLOR
      detail
   }
}

textures/qcdm3/dec_acid_side
{	
	surfaceparm nonsolid
	surfaceparm nomarks
	nopicmip
	{
		map textures/qcdm3/dec_acid_side.tga
		blendFunc add
    }
}

textures/qcdm3/trim_yellow
{
	surfaceparm nomarks
	q3map_lightSubdivide 32
        q3map_backSplash 0 4
	q3map_surfacelight 700
	nopicmip
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/trim_yellow.tga
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
	{
		map textures/qcdm3/trim_yellow_blend.tga
		blendfunc GL_ONE GL_ONE
	}
}

textures/qcdm3/k_light02b
{
	qer_editorimage textures/qcdm3/k_light02b.tga
	q3map_surfacelight 2500
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/k_light02b.tga
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
	{
		map textures/qcdm3/k_light02b_blend.tga
		blendfunc GL_ONE GL_ONE
	}
}

textures/qcdm3/ph_sq_lt
{
	qer_editorimage textures/qcdm3/ph_sq_lt.tga
	q3map_surfacelight 1500
	nopicmip
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/ph_sq_lt.tga
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
	{
		map textures/qcdm3/ph_sq_lt_blend.tga
		blendfunc GL_ONE GL_ONE
	}
}

textures/qcdm3/ph_light1
{
	qer_editorimage textures/qcdm3/ph_light1.tga
	q3map_surfacelight 1500
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/ph_light1.tga
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
	{
		map textures/qcdm3/ph_light1_blend.tga
		blendfunc GL_ONE GL_ONE
	}
}

textures/qcdm3/ph_rox_n
{
	qer_editorimage textures/qcdm3/ph_rox.tga
	surfaceparm nonsolid
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/ph_rox.tga
		rgbGen identity
		blendFunc GL_DST_COLOR GL_ZERO
	}
}

textures/qcdm3/ph_rox_s
{
	qer_editorimage textures/qcdm3/ph_rox.tga
	surfaceparm slick
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/ph_rox.tga
		rgbGen identity
		blendFunc GL_DST_COLOR GL_ZERO
	}
}

textures/qcdm3/rox0172_s
{
	qer_editorimage textures/qcdm3/rox0172.tga
	surfaceparm slick
	{
		map $lightmap
		rgbGen identity
	}
	{
		map textures/qcdm3/rox0172.tga
		rgbGen identity
		blendFunc GL_DST_COLOR GL_ZERO
	}
}

textures/qcdm3/chain-fence
{      
	q3map_vertexScale 1.50   
	cull disable
	surfaceparm trans
	surfaceparm alphashadow
	surfaceparm nolightmap  
	{
		map textures/qcdm3/chain-fence.tga 
                alphaFunc GE128
                depthWrite
		rgbGen vertex
	}
}

textures/qcdm3/hiptele
{
	qer_editorimage textures/qcdm3/hiptele.jpg
	surfaceparm noimpact
	surfaceparm nolightmap
	cull none
	nopicmip
	surfaceparm nonsolid
	surfaceparm nomarks
	q3map_surfacelight 140
	q3map_backSplash 10 25	
	{
		map textures/qcdm3/hiptele_scan2.jpg
		blendfunc add		
		rgbGen identity
		tcMod Scroll 0 -0.3
		rgbGen wave sin .8 .2 0 1		
	}			
	{
		map textures/qcdm3/hiptele.jpg
		blendfunc add		
		rgbGen identity
		rgbGen wave sin .8 .2 0 1
	}
}

//tele dests
textures/qcdm3/t_arrdec
{
	qer_editorimage textures/qcdm3/t_arrdec.tga
	surfaceparm trans
	surfaceparm nomarks
	q3map_surfacelight 300
	polygonoffset
	nopicmip
	cull none

	{
		map textures/qcdm3/t_arrdec.tga
		blendFunc add
		rgbGen wave triangle 0.4 0.25 0 0.4
	}
}

textures/qcdm3/ra-dec
{
	qer_editorimage textures/qcdm3/ra-dec.tga
	surfaceparm trans
	surfaceparm nomarks
	q3map_surfacelight 300
	polygonoffset
	nopicmip
	cull none

	{
		map textures/qcdm3/ra-dec.tga
		blendFunc add
		rgbGen wave sin .5 .45 0 .6
	}
}

textures/qcdm3/mh-dec
{
	qer_editorimage textures/qcdm3/mh-dec.tga
	surfaceparm trans
	surfaceparm nomarks
	q3map_surfacelight 300
	polygonoffset
	nopicmip
	cull none

	{
		map textures/qcdm3/mh-dec.tga
		blendFunc add
		rgbGen wave sin .5 .45 0 .6
	}
}

textures/qcdm3/ya-dec
{
	qer_editorimage textures/qcdm3/ya-dec.tga
	surfaceparm trans
	surfaceparm nomarks
	q3map_surfacelight 300
	polygonoffset
	nopicmip
	cull none

	{
		map textures/qcdm3/ya-dec.tga
		blendFunc add
		rgbGen wave sin .5 .45 0 .6
	}
}

textures/qcdm3/ws-dec
{
	qer_editorimage textures/qcdm3/ws-dec.tga
	surfaceparm trans
	surfaceparm nomarks
	q3map_surfacelight 300
	polygonoffset
	sort 10
	nopicmip
	cull none

	{
		map textures/qcdm3/ws-dec.tga
		blendFunc add
		rgbGen wave sin .5 .45 0 .6
	}
}

textures/vale/orange_line_light
{
	qer_editorimage textures/vale/orange_line.tga
	dpoffsetmapping - 1 match8 19
	
	q3map_bounceScale 1.25

	q3map_surfacelight 2500
	q3map_lightSubdivide 64

	{
		map textures/vale/orange_line.tga
		

	}
	{
		map $lightmap
		rgbGen identity
		tcGen lightmap
		blendfunc filter
	}
}

textures/vale/white_line_light
{
	qer_editorimage textures/vale/white_line.tga
	dpoffsetmapping - 1 match8 19
	
	q3map_bounceScale 1.25

	q3map_surfacelight 1500
	q3map_lightSubdivide 64

	{
		map textures/vale/white_line.tga
		

	}
	{
		map $lightmap
		rgbGen identity
		tcGen lightmap
		blendfunc filter
	}
}

textures/vale/slime
{
	qer_editorimage textures/liquids_slime/slime1.tga
	qer_trans 10
	q3map_globaltexture
	surfaceparm nonsolid
	surfaceparm slime
	q3map_lightmapSampleOffset -8
	q3map_surfacelight 100
	q3map_backSplash 0 4
	cull disable
	tesssize 32
	nopicmip
	deformVertexes wave 40 sin 0 2.5 0 0.05
    {
        	map textures/liquids_slime/slime1.tga
        	blendFunc blend
        	rgbGen const ( 0.35 0.35 0.35 )
        	tcMod turb 0 0.02 0 0.17
    }
    dp_water 0.1 0.8  3 3  1 1 0.5  1 1 0.5  0.75
}
