textures/skies/xowoken_sky
{
	qer_editorimage textures/skies/xowoken_sky.tga

	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm nomarks
	surfaceparm sky

	//q3map_sun <red> <green> <blue> <intensity> <degrees> <elevation>
	q3map_sun 1 1 1 120 225 30
	q3map_skylight 60 4
	skyparms env/xowoken_sky/xowoken_sky - -
}

textures/map_xowoken/liquid_surfacelight
{
	qer_editorimage textures/liquids_lava/lava0_purple.tga
	surfaceparm lava
	surfaceparm noimpact
	surfaceparm trans
	cull disable
	deformVertexes wave 150.0 sin 2 1 0.25 0.1
	Q3map_TessSize 128
	q3map_surfacelight 350
	q3map_lightSubdivide 64
	{
		map textures/liquids_lava/lava0_purple.tga
		blendfunc GL_SRC_ALPHA GL_ONE
	}
}

textures/map_xowoken/ivy
{
	qer_editorimage textures/map_xowoken/ivy

	dpglossintensitymod 1.5
	dpglossexponentmod 4
	q3map_bouncescale 1.25

	surfaceparm trans
	surfaceparm nonsolid
	polygonOffset
	cull none
	nopicmip

	{
		map textures/map_xowoken/ivy
		blendFunc blend
		rgbgen identity
	}
	{
		map $lightmap
		rgbGen identity
		tcGen lightmap
		blendfunc filter
	}
}

